# Front-end for 'MyFinances' signle page application

The application is built using TypeScript, React, Redux, GraphQL (Apollo). Deployable as a SPA to AWS S3. It uses AWS Cognito as authorization provider.

## Install required modules/packages

```
$ npm install
```

## Run the app in dev mode

```
$ npm start
```

## Build the app

```
$ npm run build
```

## Format the code using Prettier

```
$ npm run prettier
```
