import * as React from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router';
import { Link } from 'react-router-dom';
import { gql } from 'apollo-boost';
import { withApollo, WithApolloClient } from 'react-apollo';
import { RootState } from 'app/reducers';
import { setIdToken } from 'app/actions';
import {
  getLocationSearchParameters,
  getLocationHashParameters,
  isEmpty,
  goToCurrentPathRoot
} from 'app/utils';
import { cognitoLoginFormUrl } from 'app/constants';

const Page1 = () => <div>Page 1....</div>;
const Page2 = () => <div>Page 2....</div>;
const NoMatch = () => <div>Page not found</div>;
const isValidIdToken = gql`
  query isValidIdToken {
    isValidIdToken
  }
`;

interface StateProps {
  idToken?: string;
}

interface DispatchProps {
  setToken: (token?: string) => void;
}

interface Props extends StateProps, DispatchProps, WithApolloClient<{}> {}

interface State {
  isLoggedIn: boolean;
  isLoggingIn: boolean;
}

export class App extends React.PureComponent<Props, State> {
  static defaultProps: Partial<Props> = {};

  constructor(props: Props) {
    super(props);

    this.logOut = this.logOut.bind(this);

    this.state = {
      isLoggedIn: false,
      isLoggingIn: false
    };
  }

  componentDidMount() {
    const { action } = getLocationSearchParameters();
    const { idToken, setToken, client } = this.props;
    let isOperationComplete = false;

    if (action === 'login') {
      const { id_token } = getLocationHashParameters();

      if (id_token) {
        setToken(id_token);
        isOperationComplete = true;
      }
    } else if (action === 'logout') {
      setToken('');
      isOperationComplete = true;
    }

    if (isOperationComplete) {
      goToCurrentPathRoot();
      return;
    }

    if (!isEmpty(idToken)) {
      this.setState({
        isLoggingIn: true
      });
      client
        .query({
          query: isValidIdToken,
          context: {
            headers: { Authorization: idToken }
          }
        })
        .then(({ data }) =>
          this.setState({
            isLoggingIn: false,
            isLoggedIn: true
          })
        )
        .catch((error) =>
          this.setState({
            isLoggingIn: false,
            isLoggedIn: false
          })
        );
    }
  }

  logOut(event: React.SyntheticEvent<HTMLAnchorElement>) {
    const { setToken } = this.props;

    event.preventDefault();
    setToken('');
    this.setState({
      isLoggingIn: false,
      isLoggedIn: false
    });
  }

  render() {
    const { isLoggedIn, isLoggingIn } = this.state;

    return (
      <React.Fragment>
        <header>MyFinances App</header>
        {isLoggedIn && (
          <React.Fragment>
            <Switch>
              <Route path="/" exact render={() => null} />
              <Route path="/page1" component={Page1} />
              <Route path="/page2" component={Page2} />
              <Route component={NoMatch} />
            </Switch>
            <nav>
              <Link to="/">Home</Link>
              &nbsp;|&nbsp;
              <Link to="/page1">Page 1</Link>
              &nbsp;|&nbsp;
              <Link to="/page2">Page 2</Link>
              &nbsp;|&nbsp;
              <a href="#" onClick={this.logOut}>
                Logout
              </a>
            </nav>
            <hr />
          </React.Fragment>
        )}
        {!isLoggedIn &&
          !isLoggingIn && (
            <nav>
              <a href={cognitoLoginFormUrl} rel="noreferrer" target="_self">
                Login
              </a>
            </nav>
          )}
        {!isLoggedIn && isLoggingIn && <main>Logging in...</main>}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state: RootState): StateProps => {
  return {
    idToken: state.idToken
  };
};

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => {
  return {
    setToken: (token?: string) => {
      dispatch(setIdToken(token));
    }
  };
};

export default connect<StateProps, DispatchProps, {}, RootState>(
  mapStateToProps,
  mapDispatchToProps
)(withApollo(App));
