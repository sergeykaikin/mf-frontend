import { Store as ReduxStore, createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { routerMiddleware } from 'react-router-redux';
import { persistStore, persistReducer, Persistor } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { History } from 'history';
import { logger } from 'app/middleware';
import { RootState, rootReducer } from 'app/reducers';

export interface Store {
  store: ReduxStore<RootState>;
  persistor: Persistor;
}

export function configureStore(history: History, initialState?: RootState): Store {
  let middleware;

  if (process.env.NODE_ENV === 'production') {
    middleware = applyMiddleware(routerMiddleware(history));
  } else {
    middleware = composeWithDevTools(applyMiddleware(logger, routerMiddleware(history)));
  }

  const persistedReducer = persistReducer({ key: 'idToken', storage }, rootReducer);
  const store = createStore(persistedReducer as any, initialState as any, middleware) as ReduxStore<
    RootState
  >;
  const persistor = persistStore(store);

  if (module.hot) {
    module.hot.accept('app/reducers', () => {
      const nextReducer = require('app/reducers');
      store.replaceReducer(nextReducer);
    });
  }

  return {
    store,
    persistor
  };
}
