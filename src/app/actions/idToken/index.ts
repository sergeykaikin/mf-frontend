import { createAction } from 'redux-actions';

export const SET_ID_TOKEN = 'SET_ID_TOKEN';

export const setIdToken = createAction<string | undefined, string | undefined>(
  SET_ID_TOKEN,
  (idToken?: string) => idToken
);
