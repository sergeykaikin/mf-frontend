import { StringMap } from 'app/models';

const decodeURIPart = (value: string) => {
  return decodeURIComponent(value.replace(/\+/g, ' '));
};

const getURIPartParameters = (query: string) => {
  const params: StringMap = {};
  let pairs;
  let i;
  let length;

  for (pairs = query.split('&'), i = 0, length = pairs.length; i < length; i++) {
    let value = pairs[i];
    let index = value.indexOf('=');

    if (-1 < index) {
      params[decodeURIPart(value.slice(0, index))] = decodeURIPart(value.slice(index + 1));
    } else if (value.length) {
      params[decodeURIPart(value)] = '';
    }
  }

  return params;
};

export const getLocationSearchParameters = () => {
  let query = window.location.search;

  if (query.charAt(0) === '?') {
    query = query.slice(1);
  }

  return getURIPartParameters(query);
};

export const getLocationHashParameters = () => {
  let query = window.location.hash;

  if (query.charAt(0) === '#') {
    query = query.slice(1);
  }

  return getURIPartParameters(query);
};

export const isEmpty = (value: any) =>
  value === undefined ||
  value === null ||
  (typeof value === 'string' && value.trim() === '') ||
  (value instanceof Array && value.length === 0) ||
  (typeof value === 'object' && Object.keys(value).length === 0);

export const goToCurrentPathRoot = () => {
  const href = window.location.href.toString();

  window.location.replace(
    href.replace(window.location.hash, '').replace(window.location.search, '')
  );
};
