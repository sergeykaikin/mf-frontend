import { combineReducers } from 'redux';
import { routerReducer, RouterState } from 'react-router-redux';
import { RootState } from './state';
import { reducer as idTokenReducer } from './idToken';

export { RootState, RouterState };

// NOTE: current type definition of Reducer in 'react-router-redux' and 'redux-actions' module
// doesn't go well with redux@4
export const rootReducer = combineReducers<RootState>({
  idToken: idTokenReducer,
  router: routerReducer as any
});
