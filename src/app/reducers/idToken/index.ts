import { Action, handleActions } from 'redux-actions';
import { SET_ID_TOKEN } from 'app/actions/idToken';

export const initialState: string = '';

export function setIdToken(
  state: string | undefined,
  action: Action<string | undefined>
): string | undefined {
  return action.payload;
}

export const reducer = handleActions<string | undefined, any>(
  {
    [SET_ID_TOKEN]: setIdToken
  },
  initialState
);
