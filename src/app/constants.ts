// TODO: get these values using CloudFront template
export const cognitoUserPoolClientId = '4lckcfs90657ofeqklv6ihv0ga';
export const cognitoClientCallbackUrl =
  'https://s3-eu-west-1.amazonaws.com/my-finances-app/index.html?action=login';
export const cognitoLoginFormUrl = `https://appsync-test.auth.eu-west-1.amazoncognito.com/login?client_id=${cognitoUserPoolClientId}&redirect_uri=${cognitoClientCallbackUrl}&response_type=token`;
